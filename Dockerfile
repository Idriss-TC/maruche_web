# FROM openjdk:18-alpine
# VOLUME /tmp
# COPY firebase-config.json /app/
# COPY maruche-0.0.1-SNAPSHOT app.jar
# ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]

# Utilisez l'image OpenJDK 17 Alpine comme base
FROM openjdk:17-alpine

# Installez Maven (ou d'autres outils) si nécessaire
RUN apk update && apk add maven

# Copiez votre script de pipeline (ou scripts pour chaque étape) dans l'image
COPY pipeline-script.sh /pipeline-script.sh

# Configurez votre script d'entrée (CMD ou ENTRYPOINT) pour exécuter les étapes de votre pipeline
CMD ["/pipeline-script.sh"]