package com.example.maruche.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class  FirebaseAuthenticationServiceTest {

    private FirebaseAuthenticationService authService;
    private RestTemplate restTemplate;

    @BeforeEach
    public void setUp() {
        restTemplate = mock(RestTemplate.class);
        authService = new FirebaseAuthenticationService();
    }

    @Test
    public void testAuthenticate() {
        String expectedResponseInvalidPassword = "400 Bad Request: {\n" +
                "  \"error\": {\n" +
                "    \"code\": 400,\n" +
                "    \"message\": \"INVALID_PASSWORD\",\n" +
                "    \"errors\": [\n" +
                "      {\n" +
                "        \"message\": \"INVALID_PASSWORD\",\n" +
                "        \"domain\": \"global\",\n" +
                "        \"reason\": \"invalid\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";
        
        String expectedResponseEmailNotFound = "400 Bad Request: {\n" +
                "  \"error\": {\n" +
                "    \"code\": 400,\n" +
                "    \"message\": \"EMAIL_NOT_FOUND\",\n" +
                "    \"errors\": [\n" +
                "      {\n" +
                "        \"message\": \"EMAIL_NOT_FOUND\",\n" +
                "        \"domain\": \"global\",\n" +
                "        \"reason\": \"invalid\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";

        ResponseEntity<String> responseEntityInvalidPassword = new ResponseEntity<>(expectedResponseInvalidPassword, HttpStatus.BAD_REQUEST);
        ResponseEntity<String> responseEntityEmailNotFound = new ResponseEntity<>(expectedResponseEmailNotFound, HttpStatus.BAD_REQUEST);
        
        when(restTemplate.postForEntity(eq(FirebaseAuthenticationService.FIREBASE_SIGN_IN_URL), any(), eq(String.class)))
            .thenReturn(responseEntityInvalidPassword) // Simuler une réponse d'erreur INVALID_PASSWORD
            .thenReturn(responseEntityEmailNotFound);  // Simuler une réponse d'erreur EMAIL_NOT_FOUND

        String actualResponse = authService.authenticate("waliddadi75@gmail.com", "test123");

        assertNotEquals(expectedResponseInvalidPassword, actualResponse);
        assertNotEquals(expectedResponseEmailNotFound, actualResponse);
    }


    @Test
    public void testCreateUser() {
        String expectedResponse = "user created response";
        ResponseEntity<String> responseEntitySuccess = new ResponseEntity<>(expectedResponse, HttpStatus.OK);

        String expectedResponseMissingPassword = "400 Bad Request: {\n" +
                "  \"error\": {\n" +
                "    \"code\": 400,\n" +
                "    \"message\": \"MISSING_PASSWORD\",\n" +
                "    \"errors\": [\n" +
                "      {\n" +
                "        \"message\": \"MISSING_PASSWORD\",\n" +
                "        \"domain\": \"global\",\n" +
                "        \"reason\": \"invalid\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";

        String expectedResponseInvalidEmail = "400 Bad Request: {\n" +
                "  \"error\": {\n" +
                "    \"code\": 400,\n" +
                "    \"message\": \"INVALID_EMAIL\",\n" +
                "    \"errors\": [\n" +
                "      {\n" +
                "        \"message\": \"INVALID_EMAIL\",\n" +
                "        \"domain\": \"global\",\n" +
                "        \"reason\": \"invalid\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";

        String expectedResponseMissingEmail = "400 Bad Request: {\n" +
                "  \"error\": {\n" +
                "    \"code\": 400,\n" +
                "    \"message\": \"MISSING_EMAIL\",\n" +
                "    \"errors\": [\n" +
                "      {\n" +
                "        \"message\": \"MISSING_EMAIL\",\n" +
                "        \"domain\": \"global\",\n" +
                "        \"reason\": \"invalid\"\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";

        ResponseEntity<String> responseEntityMissingPassword = new ResponseEntity<>(expectedResponseMissingPassword, HttpStatus.BAD_REQUEST);
        ResponseEntity<String> responseEntityInvalidEmail = new ResponseEntity<>(expectedResponseInvalidEmail, HttpStatus.BAD_REQUEST);
        ResponseEntity<String> responseEntityMissingEmail = new ResponseEntity<>(expectedResponseMissingEmail, HttpStatus.BAD_REQUEST);

        when(restTemplate.postForEntity(eq(FirebaseAuthenticationService.FIREBASE_SIGN_UP_URL), any(), eq(String.class)))
            .thenReturn(responseEntitySuccess)      // Simuler une réponse réussie
            .thenReturn(responseEntityMissingPassword)  // Simuler une réponse d'erreur MISSING_PASSWORD
            .thenReturn(responseEntityInvalidEmail)    // Simuler une réponse d'erreur INVALID_EMAIL
            .thenReturn(responseEntityMissingEmail);   // Simuler une réponse d'erreur MISSING_EMAIL

        String actualResponseSuccess = authService.createUser("newuser9123@gmail.com", "test123");

        assertNotEquals(expectedResponseMissingPassword, actualResponseSuccess);
        assertNotEquals(expectedResponseInvalidEmail, actualResponseSuccess);
        assertNotEquals(expectedResponseMissingEmail, actualResponseSuccess);
    }

    // Helper method to match any payload in the tests
    private String anyPayload() {
        return org.mockito.ArgumentMatchers.anyString();
    }
}
