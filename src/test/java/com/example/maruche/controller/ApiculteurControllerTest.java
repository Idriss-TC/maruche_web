// package com.example.maruche.controller;

// import static org.junit.jupiter.api.Assertions.*;
// import static org.mockito.Mockito.*;

// import java.util.concurrent.ExecutionException;

// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;

// import com.example.maruche.model.Apiculteur;
// import com.google.cloud.firestore.CollectionReference;
// import com.google.cloud.firestore.DocumentSnapshot;
// import com.google.cloud.firestore.Firestore;

// public class ApiculteurControllerTest {

//     private ApiculteurController apiculteurController;
//     private Firestore firestore;
//     private CollectionReference collectionReference;

//     @BeforeEach
//     public void setUp() {
//         firestore = mock(Firestore.class);
//         collectionReference = mock(CollectionReference.class);
//         apiculteurController = new ApiculteurController();
//     }

//     @Test
//     public void testUpdateApiculteur() throws ExecutionException, InterruptedException {
//         String id = "PdS5f71TeRfPy6bYW2qZvvVIcbR2";

//         Apiculteur updatedApiculteur = new Apiculteur();
//         updatedApiculteur.setLocalId(id);
//         updatedApiculteur.setAdresse("123 Rue Bee");
//         updatedApiculteur.setEmail("john@example.com");
//         updatedApiculteur.setIdentifiant("john_doe");
//         updatedApiculteur.setNom("Doe");
//         updatedApiculteur.setPrenom("John");

//         DocumentSnapshot documentSnapshot = mock(DocumentSnapshot.class);
//         when(documentSnapshot.exists()).thenReturn(true);
//         when(firestore.collection("Apiculteur").document(id).get().get()).thenReturn(documentSnapshot);
        
//         ResponseEntity<Apiculteur> responseEntity = apiculteurController.updateApiculteur(id, updatedApiculteur);

//         assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//         assertEquals(updatedApiculteur, responseEntity.getBody());
//     }

//     @Test
//     public void testUpdateApiculteurNotFound() throws ExecutionException, InterruptedException {
//         String id = "apiculteur123";
//         Apiculteur updatedApiculteur = new Apiculteur(/* initialize your data here */);

//         DocumentSnapshot documentSnapshot = mock(DocumentSnapshot.class);
//         when(documentSnapshot.exists()).thenReturn(false);
//         when(firestore.collection("Apiculteur").document(id).get().get()).thenReturn(documentSnapshot);

//         ResponseEntity<Apiculteur> responseEntity = apiculteurController.updateApiculteur(id, updatedApiculteur);

//         assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
//         assertNull(responseEntity.getBody());
//     }

//     @Test
//     public void testDeleteApiculteur() throws ExecutionException, InterruptedException {
//         String id = "apiculteur123";

//         DocumentSnapshot documentSnapshot = mock(DocumentSnapshot.class);
//         when(documentSnapshot.exists()).thenReturn(true);
//         when(firestore.collection("Apiculteur").document(id).get().get()).thenReturn(documentSnapshot);
        
//         ResponseEntity<Void> responseEntity = apiculteurController.deleteApiculteur(id);

//         assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//         assertNull(responseEntity.getBody());
//     }

//     @Test
//     public void testDeleteApiculteurNotFound() throws ExecutionException, InterruptedException {
//         String id = "apiculteur123";

//         DocumentSnapshot documentSnapshot = mock(DocumentSnapshot.class);
//         when(documentSnapshot.exists()).thenReturn(false);
//         when(firestore.collection("Apiculteur").document(id).get().get()).thenReturn(documentSnapshot);

//         ResponseEntity<Void> responseEntity = apiculteurController.deleteApiculteur(id);

//         assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
//         assertNull(responseEntity.getBody());
//     }
// }
