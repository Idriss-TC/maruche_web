document.addEventListener("DOMContentLoaded", function () {
    const loginForm = document.getElementById("login-form");

    loginForm.addEventListener("submit", async function (event) {
        event.preventDefault();

        const email = document.getElementById("email").value;
        const password = document.getElementById("password").value;

        try {
            const response = await fetch("/auth/login", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ email, password }),
            });
            
            if (response.ok) {
                const data = await response.json(); // Attendre la réponse JSON
                console.log("Utilisateur authentifié avec succès!");
                console.log(data.localId); // Utiliser data.localId
                
                // Requête pour les données de l'administrateur
                const adminResponse = await fetch("/auth/admin/"+data.localId, {
                    method: "GET",
                    headers: {
                        "Accept": "application/json",
                    },
                });
                if (adminResponse.ok) {
                    const adminData = await adminResponse.json(); // Attendre la réponse JSON pour l'administrateur
                    console.log("Données de l'administrateur:", adminData);
                } else {
                    // Si la requête pour l'administrateur échoue, tenter la requête pour l'apiculteur
                    const apiculteurResponse = await fetch("/auth/apiculteur/"+data.localId, {
                        method: "GET",
                        headers: {
                            "Accept": "application/json",
                        },
                    });
                    if (apiculteurResponse.ok) {
                        const apiculteurData = await apiculteurResponse.json(); // Attendre la réponse JSON pour l'apiculteur
                        console.log("Données de l'apiculteur:", apiculteurData);
                    } else {
                        console.error("Erreur lors de la récupération des données de l'apiculteur");
                    }
                }
            } else {
                console.error("Erreur lors de l'authentification");
            }
        } catch (error) {
            console.error("Erreur lors de l'authentification", error);
        }
    });
});
