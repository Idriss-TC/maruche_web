package com.example.maruche.model;

// AuthRequest.java
public class Admin {

    private String Email;
    private String Identifiant;
    private String Nom;
    private String Prenom;
    public String getEmail() {
        return Email;
    }
    public void setEmail(String email) {
        Email = email;
    }
    public String getIdentifiant() {
        return Identifiant;
    }
    public void setIdentifiant(String identifiant) {
        Identifiant = identifiant;
    }
    public String getNom() {
        return Nom;
    }
    public void setNom(String nom) {
        Nom = nom;
    }
    public String getPrenom() {
        return Prenom;
    }
    public void setPrenom(String prenom) {
        Prenom = prenom;
    }
    
}