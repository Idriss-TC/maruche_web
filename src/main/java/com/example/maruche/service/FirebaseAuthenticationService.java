package com.example.maruche.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FirebaseAuthenticationService {

    static final String FIREBASE_SIGN_IN_URL = 
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBWrSb1mgr38_GIzhloGWzBuggih4ymH4U";

    public String authenticate(String email, String password) {
        RestTemplate restTemplate = new RestTemplate();

        String payload = "{"
            + "\"email\":\"" + email + "\","
            + "\"password\":\"" + password + "\","
            + "\"returnSecureToken\":true"
            + "}";

        ResponseEntity<String> response = restTemplate.postForEntity(FIREBASE_SIGN_IN_URL, payload, String.class);

        return response.getBody();
    }

    static final String FIREBASE_SIGN_UP_URL = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBWrSb1mgr38_GIzhloGWzBuggih4ymH4U";

    public String createUser(String email, String password) {
        RestTemplate restTemplate = new RestTemplate();

        String payload = "{"
            + "\"email\":\"" + email + "\","
            + "\"password\":\"" + password + "\","
            + "\"returnSecureToken\":true"
            + "}";

        ResponseEntity<String> response = restTemplate.postForEntity(FIREBASE_SIGN_UP_URL, payload, String.class);

        return response.getBody();
    }

    // Autres méthodes de gestion de l'authentification...
}