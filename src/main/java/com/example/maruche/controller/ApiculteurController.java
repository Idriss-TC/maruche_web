package com.example.maruche.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.maruche.model.Apiculteur;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;

@Controller
public class ApiculteurController {

    @GetMapping("/apiculteurs")
    public ResponseEntity<List<Apiculteur>> getAllApiculteurs() throws InterruptedException, ExecutionException {
        Firestore firestore = FirestoreClient.getFirestore();
        // Récupérer la collection Apiculteur depuis Firestore
        ApiFuture<QuerySnapshot> query = firestore.collection("Apiculteur").get();

        // Obtenir les documents de la collection
        List<QueryDocumentSnapshot> documents = query.get().getDocuments();

        // Si la collection est vide, renvoyer une réponse 404
        if (documents.isEmpty()) {
            return new ResponseEntity<List<Apiculteur>>(null, null, HttpStatus.SC_NOT_FOUND);
        }

        // Créer une liste d'objets Apiculteur à partir des documents
        List<Apiculteur> apiculteurs = new ArrayList<>();
        for (QueryDocumentSnapshot document : documents) {
            Apiculteur apiculteur = document.toObject(Apiculteur.class);
            apiculteurs.add(apiculteur);
        }

        // Renvoyer la liste des apiculteurs
        return new ResponseEntity<List<Apiculteur>>(apiculteurs, null, HttpStatus.SC_OK);
    }

    @PutMapping("/apiculteurs/{id}")
    public ResponseEntity<Apiculteur> updateApiculteur(@PathVariable String id, @RequestBody Apiculteur updatedApiculteur) throws InterruptedException, ExecutionException {
        Firestore firestore = FirestoreClient.getFirestore();
        
        // Vérifier si l'apiculteur existe dans Firestore
        DocumentSnapshot document = firestore.collection("Apiculteur").document(id).get().get();
        if (!document.exists()) {
            return new ResponseEntity<Apiculteur>(updatedApiculteur, null, HttpStatus.SC_NOT_FOUND);
        }

        // Mettre à jour l'apiculteur avec les nouvelles données
        firestore.collection("Apiculteur").document(id).set(updatedApiculteur);

        // Renvoyer l'apiculteur mis à jour
        return new ResponseEntity<Apiculteur>(updatedApiculteur, null, HttpStatus.SC_OK);
    }

    @DeleteMapping("/apiculteurs/{id}")
    public ResponseEntity<Void> deleteApiculteur(@PathVariable String id) throws InterruptedException, ExecutionException {
        Firestore firestore = FirestoreClient.getFirestore();
        
        // Vérifier si l'apiculteur existe dans Firestore
        DocumentSnapshot document = firestore.collection("Apiculteur").document(id).get().get();
        if (!document.exists()) {
            return new ResponseEntity<Void>(null, null, HttpStatus.SC_NOT_FOUND);
        }

        // Supprimer l'apiculteur
        firestore.collection("Apiculteur").document(id).delete();

        // Renvoyer une réponse OK
        return new ResponseEntity<Void>(null, null, HttpStatus.SC_OK);
    }
}
