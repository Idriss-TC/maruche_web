package com.example.maruche.controller;

// import java.util.ArrayList;
// import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;

import com.example.maruche.model.Admin;
import com.example.maruche.model.Apiculteur;
import com.example.maruche.model.AuthRequest;
import com.example.maruche.model.CreateUserRequest;
import com.example.maruche.model.CreateUserResponse;
import com.example.maruche.service.FirebaseAuthenticationService;
// import com.google.api.core.ApiFuture;
// import com.google.cloud.firestore.CollectionReference;
// import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
// import com.google.cloud.firestore.Query;
// import com.google.cloud.firestore.QueryDocumentSnapshot;
// import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;

@Controller
@RequestMapping("/auth")
public class FirebaseAuthController {

    @Autowired
    private FirebaseAuthenticationService authService;

    @PostMapping("/login")
    public ResponseEntity<String> loginUser(@RequestBody AuthRequest authRequest) {
        try {
            String response = authService.authenticate(authRequest.getEmail(), authRequest.getPassword());
            return ResponseEntity.ok(response);

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    
    @GetMapping("/admin/{id}")
    public ResponseEntity<Admin> getAdmin(@PathVariable String id) throws InterruptedException, ExecutionException {
        Firestore firestore = FirestoreClient.getFirestore();
        // Récupérer le document User de Firestore
        DocumentSnapshot document = firestore.collection("Admin").document(id).get().get();

        // Si le document n'existe pas, renvoyer une réponse 404
        if (!document.exists()) {
            return new ResponseEntity<Admin>(null, null, HttpStatus.SC_NOT_FOUND);
        }

        // Créer un objet User à partir du document
        Admin user = document.toObject(Admin.class);

        // Renvoyer le User
        return new ResponseEntity<Admin>(user, null, HttpStatus.SC_OK);
    }

    @GetMapping("/apiculteur/{id}")
    public ResponseEntity<Apiculteur> getUser(@PathVariable String id) throws InterruptedException, ExecutionException {
        Firestore firestore = FirestoreClient.getFirestore();
        // Récupérer le document User de Firestore
        DocumentSnapshot document = firestore.collection("Apiculteur").document(id).get().get();

        // Si le document n'existe pas, renvoyer une réponse 404
        if (!document.exists()) {
            return new ResponseEntity<Apiculteur>(null, null, HttpStatus.SC_NOT_FOUND);
        }

        // Créer un objet User à partir du document
        Apiculteur user = document.toObject(Apiculteur.class);

        // Renvoyer le User
        return new ResponseEntity<Apiculteur>(user, null, HttpStatus.SC_OK);
    }

    @PostMapping("/users")
    public ResponseEntity<String> createUser(@RequestBody CreateUserRequest userRequest) {
        try {
            String response = authService.createUser(userRequest.getEmail(), userRequest.getPassword());
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/createUserInFirestore/{id}")
    public ResponseEntity<String> createUserInFirestore(@RequestBody CreateUserResponse createUserResponse, @PathVariable String id) {
        try {
            Firestore firestore = FirestoreClient.getFirestore();
            
            // Créer un nouvel utilisateur dans Firestore en utilisant les champs du corps de la requête
            Apiculteur newUser = new Apiculteur();
            newUser.setLocalId(createUserResponse.getLocalId());
            newUser.setEmail(createUserResponse.getEmail());
            newUser.setPassword(createUserResponse.getPassword());
            // Ajoutez d'autres champs en fonction de votre modèle
            
            // Ajouter l'utilisateur à Firestore en utilisant l'ID passé en tant que chemin du document
            firestore.collection("Apiculteur").document(id).set(newUser);
            
            return ResponseEntity.ok("User created in Firestore successfully");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/pagelogin")
    public String showLoginPage() {
        return "connexion/login"; // Retourne le nom de la vue HTML sans extension
    }
}