package com.example.maruche.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.maruche.model.AuthRequest;

@Controller
@RequestMapping("/apiculteur")
public class RuchersController {

    @PostMapping("/login")
    public ResponseEntity<String> loginUser(@RequestBody AuthRequest authRequest) {
        try {
            return ResponseEntity.ok("response");

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/pagehome")
    public String showLoginPage() {
        return "home/ruchers"; // Retourne le nom de la vue HTML sans extension
    }
}