package com.example.maruche;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarucheApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarucheApplication.class, args);
	}

}
